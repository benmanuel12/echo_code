import java.util.Scanner;
import java.net.Socket;
import java.io.IOException;
import java.io.PrintWriter;

public class echoclient {
	public static void main(String[] args) {

		final String HOST;
		final int PORT;

		if (args.length == 2) {
			HOST = args[0];
			PORT = Integer.parseInt(args[1]);
		} else {
			HOST = "localhost";
			PORT = 13;
		}

		try {
			Socket socket = new Socket(HOST, PORT);

			Scanner input = new Scanner(System.in);
			String myString = input.nextLine();

			PrintWriter output = new PrintWriter(socket.getOutputStream());
			output.println(myString);
			output.flush();

			Scanner serverResult = new Scanner(socket.getInputStream());
			System.out.println(serverResult.next());
			socket.close();

		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}
}
