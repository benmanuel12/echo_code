import java.io.*;
import java.net.*;
import java.util.Scanner;

public class echoserver {
	public static void main(String[] args) {

		final int PORT;
		if (args.length != 1) {
			System.out.println("Usage java echoserver PORT");
			System.exit(1);
		}

		PORT = Integer.parseInt(args[0]);

		try {
			Socket socket = null;
			ServerSocket serverSocket = new ServerSocket(PORT);
			System.out.println("Server started");
			System.out.println("Waiting for a client...");

			while (true) {
				socket = serverSocket.accept();
				System.out.println("Client Accepted");

				Scanner input = new Scanner(socket.getInputStream());
				String outputString = input.nextLine();

				PrintWriter output = new PrintWriter(socket.getOutputStream());
				output.println(outputString);
				output.flush();

				socket.close();
			}
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}

	}
}